@extends('demo.master')

@section('title')

    Form Sign Up

@endsection

@section('content')
        <h2>Buat Account Baru!</h2>
        <h3>Sign Up Form</h3>
        <form action="/welcome" method="post">
            @csrf
            <label for="fname">First Name : </label><br><br>
            <input type="text" name="fname"><br><br>
            <label for="lname">Last Name : </label><br><br>
            <input type="text" name="lname"><br><br>
            <label for="gender">Gender : </label><br><br>
            <input type="radio" id="male" name="gender" value="Male">
            <label for="male">Male</label><br>
            <input type="radio" id="fmale" name="gender" value="Female">
            <label for="fmale">Female</label><br>
            <input type="radio" id="other" name="gender" value="Other">
            <label for="male">Other</label><br><br>
            <label for="nationality">Nationality</label><br><br>
            <select name="nationality" id="nationality">
                <option value="indonesia">Indonesia</option>
                <option value="amerika">Amerika</option>
                <option value="inggris">Inggris</option>
            </select><br><br>
            <label for="lang_spoken">Language Spoken : </label><br><br>
            <input type="checkbox" id="b_indo" name="lang_spoken" value="Bahasa Indonesia">
            <label for="b_indo">Bahasa Indonesia</label><br>
            <input type="checkbox" id="eng" name="lang_spoken" value="English">
            <label for="eng">English</label><br>
            <input type="checkbox" id="other1" name="lang_spoken" value="Other">
            <label for="other1">Other</label><br><br>
            <label for="bio">Bio : </label><br><br>
            <textarea id="bio" name="bio" rows="10" cols="50"></textarea><br>
            <input type="submit" name="submit" value="Sign Up"><br>
        </form>
		<br><br>
    
@endsection